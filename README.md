# README #

### What is this repository for? ###

This project consists of a scrapy based web crawler, that downloads Budapest related data from ingatlan.com(contains hungarian housing data) into an elasticsearch cluster for further analyses. Currently it contains a Kibana dashboard for basic stats.

### Setup
The followings need to be done to set up the project:

* Gather the needed python packages:

	pip install -r requirements.txt

* Set up the elastic clsuter and kibana:

	docker-compose up -d

* Crawl data from ingatlanok.com into the elastic cluster(it takes some time):

	scrapy crawl ingatlanok

### Plans:

- providing a jupyter notebook for furter more advanced analyses.

