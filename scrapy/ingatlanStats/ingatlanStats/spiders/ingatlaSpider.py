import scrapy
import re
import logging
from scrapy.loader import ItemLoader
from scrapy.selector import Selector
from ingatlanStats.items import IngatlanstatsItem
from ingatlanStats.helper import formatter


class ingatlanSpider(scrapy.Spider):
    """Crawls data from ingatlan.com"""
    name = "ingatlanok"
    start_urls = [
        'https://ingatlan.com/szukites/elado+lakas+budapest'
    ]

    def parse(self, response):
        s = Selector(response)
        listings = s.xpath('//body/div[@class="site__content"]/div/main/div/div/div[@class="listing__card"]/a')
        for l in listings:
            item = IngatlanstatsItem()
            try:
                item['price'] = formatter('price', l.xpath(
                    './/header/div/div[@class="listing__price"]/div[@class="price"]/text()').extract())
                item['address'], item['district'] = formatter('address', l.xpath(
                    './/header/div/div[@class="listing__address"]/text()').extract())
                item['size'] = formatter('size', l.xpath(
                    './/div/div[@class="listing__parameter listing__data--area-size"]/text()').extract())
                item['roomCount'] = formatter('roomCount', l.xpath(
                    './/div/div[@class="listing__parameter listing__data--room-count"]/text()').extract())
                yield item
            except Exception as e:
                output = 'Failed to extract item: "{}".'.format(e)
                logging.critical(output)

        nextPage = response.xpath(
            '//body/div[@class="site__content"]/div[@class="resultspage js-resultspage"]/main/div[1]/nav/div/div[3]/a/@href').extract()[
            0]
        if nextPage is not None:
            yield response.follow(nextPage, callback=self.parse)
