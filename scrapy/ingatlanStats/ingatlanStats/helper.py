import re


class unknownType(Exception):
    pass;


class notValidString(Exception):
    pass;


def formatter(type, value):
    if not value:
        raise notValidString('List is empty for "{}".'.format(type))
    value = value[0].strip()
    if type == 'price':
        value = "".join(value.split())
        m = re.match('\d*\.?\d+', value)
        if not m:
            raise notValidString('"{}" is not a valid price format.'.format(value))
        return float(m.group(0))
    elif type == 'address':
        address, district = value.split(',')
        district = district[:-9].strip()
        return address, district
    elif type == 'size':
        return float(value[:-11])
    elif type == 'roomCount':
        m = re.match('(\d+)(\s\+\s(\d+)\sfél){0,1}\sszoba', value)
        if not m:
            raise notValidString('"{}" is not a valid roomCount format.'.format(value))
        if m.group(3) is None:
            return float(m.group(1))
        else:
            return float(m.group(1)) + float(m.group(3))/2
    else:
        raise unknownType('No such type.')
